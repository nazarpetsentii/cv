terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}
provider "aws" {
      region = "eu-central-1"
}
resource "aws_vpc" "custom" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "terraform"
  }
}
resource "aws_internet_gateway" "gateway" {
  vpc_id = aws_vpc.custom.id

  tags = {
    Name = "terraform"
  }
}
resource "aws_route_table" "route_table" {
  vpc_id = aws_vpc.custom.id

# Means that all traffic will be sent to gateway from this subnet (i.g to the Internet).
# A route table contains a set of rules, called routes, that are used to 
# determine where network traffic from your subnet or gateway is directed.
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gateway.id 
  }

  tags = {
    Name = "route_table"
  }
}

resource "aws_subnet" "subnet1" {
  vpc_id = aws_vpc.custom.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "eu-central-1a"

  tags = {
    Name = "subnet1"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet1.id
  route_table_id = aws_route_table.route_table.id
}
resource "aws_security_group" "allow_tls" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.custom.id

  ingress {
    description = "HTTPS traffic"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "HTTP traffic"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "SSH traffic"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_web"
  }
}

# Creates an private IP which I define for an instance.
resource "aws_network_interface" "test_network_interface" {
  subnet_id       = aws_subnet.subnet1.id
  private_ips     = ["10.0.1.50"]
  security_groups = [aws_security_group.allow_tls.id]
}

# EIP enables network interface to have elastic IP.
resource "aws_eip" "one" {
  vpc                       = true
  network_interface         = aws_network_interface.test_network_interface.id
  associate_with_private_ip = "10.0.1.50"
  depends_on = [aws_internet_gateway.gateway]
}

output "server_public_ip" {
  value = aws_eip.one.public_ip
}

variable "key_name" {
}

resource "aws_instance" "test_server" {
  ami = "ami-0c960b947cbb2dd16"
  instance_type = "t2.micro"
  availability_zone = "eu-central-1a"
  key_name = var.key_name
  
  network_interface {
    device_index = 0
    network_interface_id = aws_network_interface.test_network_interface.id
}
  user_data = <<-EOF
              #!/bin/bash
              sudo apt update -y
              sudo apt install nginx -y
              sudo systemctl start apache2
              sudo bash -c 'echo your very first webserver > /var/www/html/index.html'
              EOF
  tags = {
    Name = "webserver"
  }
}

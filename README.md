# Nazar Petsentii CV webpage hosted on a configured nginx.
## The main objective is to create fully automated pipeline that imitates real development cycle.  
Was set up to go like this:  
merge reguest -> Jenkins job1 on jen slave1: copy code to **sonarqube** server and test it. In case of success - team lead receives slack notification. So then he can have a look at the code in MR.
If merge request is approved:  
*gitlab ci* builds docker image with my webpage(index.html, css and other content) upon configured nginx server -> pushes an image to the docker hub ->  gitlab ci webhook to Jenkins job2, starting another job on the slave:   
Cloning gitlab repo to the ansible server -> runs ansible playbook that deploys an app on a test server with these roles:  
    - install_docker_awscli  
    - image_pull  
    - docker_compose  
    - puppeteer_jest  
    - aws_ecr  
  
If successfully deployed on test server team of testers receieve a notification in slack that new_app_version was deployed on a test server. Please test.  
I also configured test_server to install **puppeteer-jest** and I wrote simple test that checks what is in the header and the title (I don't have much experience in writing automated tests). 
If those tests are passed -> pushes to amazon elastic container registry. Text
  
Deploys on the stage server from *ECR*. 
## Only merge request approval is done manually. That was done in purpose.
## Jenkinsfile is currently under development. Most of the jobs were configured as free style projects.

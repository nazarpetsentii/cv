FROM nginx:alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY images/ index.html style.css /usr/share/nginx/html/
